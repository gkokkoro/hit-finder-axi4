library ieee;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use work.dtpc_stream_defs.all;
library xil_defaultlib;

entity hsc_hls is
  port (
    clk            : in  std_logic;
    rst            : in  std_logic;
    s_axis_data_w  : in  dtpc_axis4_w;
    s_axis_data_r  : out dtpc_axis4_r;
    m_axis_data_w  : out dtpc_axis4_w;
    m_axis_data_r  : in  dtpc_axis4_r;
    s_axis_payload_w  : in  dtpc_axis4_w;
    s_axis_payload_r  : out dtpc_axis4_r;
    m_axis_payload_w  : out dtpc_axis4_w;
    m_axis_payload_r  : in  dtpc_axis4_r
  );
end hsc_hls;

architecture rtl of hsc_hls is
  signal header_r :dtpc_axis4_r;
  signal header_w :dtpc_axis4_w;

begin

  header_stripper_inst : entity work.header_stripper
    port map(
        clk                 => clk,
        rst                 => rst,
        s_axis_data_w       => s_axis_data_w,
        s_axis_data_r       => s_axis_data_r,
        m_axis_head_w       => header_w,
        m_axis_head_r       => header_r,
        m_axis_payload_w    => m_axis_payload_w,
        m_axis_payload_r    => m_axis_payload_r
    );
  header_combiner_inst : entity work.header_combiner
  port map(
        clk                 => clk,
        rst                 => rst,
        m_axis_data_w       => m_axis_data_w,
        m_axis_data_r       => m_axis_data_r,
        s_axis_head_w       => header_w,
        s_axis_head_r       => header_r,
        s_axis_payload_w    => s_axis_payload_w,
        s_axis_payload_r    => s_axis_payload_r
);


end rtl;

