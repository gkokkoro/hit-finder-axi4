# create a project
open_project -reset "proj"
# specify the name of the function to synthetize
set_top hit_finder
# load source code for synthesis
add_files src/core.cpp
# load source code for the testbench
add_files -tb test.cpp

# create a solution (i.e. a hardware configuration for synthesis)
open_solution -reset "solution"
# set the FPGA (VU9P), and a 320 MHz clock
set_part {xcvu9p-flga2104-2L-e}
create_clock -period 4

# end here, so that we can then open the project interactively in the gui
exit
