----------------------------------------------------------------------------------
-- Company: Rutherford Appleton Laboratory
-- Engineer: Kostas M.
--
-- Create Date: 14.06.2019 16:49:59
-- Design Name:
-- Module Name: tb_dtp_proc_tpg - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments: Modified by D. Cussans, UoBristol
--
----------------------------------------------------------------------------------
-- Needs to be compiled as VHDL-2008 to pick up HREAD

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use STD.textio.all;

use IEEE.NUMERIC_STD.all;

use ieee.math_real.uniform;
use ieee.math_real.floor;

use work.dtpc_stream_defs.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tb_dtp_proc is
  generic (
    IFILE                      : string  := "./UniqueHits_B_up_axi4s.0.txt";  -- Name of file containing input to drive to DUT
    OFILE                      : string  := "./outputFile.txt";  -- Name of file that output will be written to
    G_TOGGLE_TREADY            : boolean := true;  -- Set this flag True in order to deassert TREADY at output of entity under test                                 
    G_MAX_TREADY_DEASSERT_TIME : real    := 6.0;
    G_MAX_TREADY_REASSERT_TIME : real    := 60.0
    );
end tb_dtp_proc;

architecture tb of tb_dtp_proc is

  -----------------------------------------------------------------------
  -- Timing constants
  -----------------------------------------------------------------------
  constant CLOCK_PERIOD : time := 5 ns;
  constant T_HOLD       : time := 0.5 ns;
  constant T_STROBE     : time := CLOCK_PERIOD - (1 ns);

  signal s_eof : boolean;

  -----------------------------------------------------------------------
  -- DUT signals
  -----------------------------------------------------------------------
  --Generic signals
  signal clk   : std_logic := '0';
  signal reset : std_logic := '0';

  signal threshold : std_logic_vector(15 downto 0) := "0000000000010100"; --70:1000110

  signal s_start_reading , s_start_writing : std_logic := '0';

  signal s_from_src_to_block  : dtpc_axis4_w := DTPC_AXIS4_W_NULL;
  signal s_from_block_to_src  : dtpc_axis4_r := DTPC_AXIS4_R_NULL;
  signal s_from_block_to_sink : dtpc_axis4_w := DTPC_AXIS4_W_NULL;
  signal s_from_sink_to_block : dtpc_axis4_r := DTPC_AXIS4_R_NULL;

  signal cycleCounter : natural := 0;   -- cycle counter


  signal FIR_out_w   : dtpc_axis4_w := DTPC_AXIS4_W_NULL;
  signal FIR_out_r   : dtpc_axis4_r := DTPC_AXIS4_R_NULL;

  signal HF_out_w    : dtpc_axis4_w := DTPC_AXIS4_W_NULL;
  signal HF_out_r    : dtpc_axis4_r := DTPC_AXIS4_R_NULL;

  --temp TPG reset signals
  signal proc_end, proc_end_d, localreset : std_logic := '0';
  signal rst, hit_finder_rst : std_logic := '0';

begin

  -----------------------------------------------------------------------
  -- Instantiate the DUT
  -----------------------------------------------------------------------
  -- Use separate configuration file to specify which block....
  -- dut : dtpc_processingBlock
  --   generic map(
  --       g_ENABLE_PEDSUB     => TPG_ENABLE_PEDSUB,
  --       g_ENABLE_FIR        => TPG_ENABLE_FIR,
  --       g_ENABLE_HF         => TPG_ENABLE_HF,                
  --       g_ENABLE_PedValTX   => TPG_ENABLE_PedValTX        
  --   )
  --   port map(
  --     clk => clk,
  --     rst => reset,
  --     threshold => threshold,
  --     d   => s_from_src_to_block,
  --     q   => s_from_block_to_src,
  --     qa  => s_from_block_to_sink,
  --     da  => s_from_sink_to_block
  --   );



  -----------------------------------------------------------------------
  -- Instantiate the HSC
  -----------------------------------------------------------------------
  headerStripComb_inst : entity work.header_strip_comb
  port map(
    clk                   => clk,
    rst                   => reset,
    ----------------- Header Stripper -----------------
    --Header + ADC data
    s_axis_data_w         => s_from_src_to_block,
    s_axis_data_r         => s_from_block_to_src,
    --Single Channel TPG Input: ADC data
    singleChannelInput_w  => FIR_out_w,
    singleChannelInput_r  => FIR_out_r,
    ----------------- Header Combiner -----------------
    --Single Channel TPG Output: Hits
    singleChannelOutput_w => HF_out_w,
    singleChannelOutput_r => HF_out_r,        
    --Header + Hits    
    m_axis_data_w         => s_from_block_to_sink,
    m_axis_data_r         => s_from_sink_to_block --
  );

  -----------------------------------------------------------------------
  -- Generate a local reset
  ----------------------------------------------------------------------
  --use tlast to apply a tmp reset between packets
  --reset last 2 clks
  -- localreset <= proc_end or proc_end_d;
  -- process(clk)
  -- begin
  -- if rising_edge(clk) then
  --     if s_from_sink_to_block.tready = '1' then
  --         proc_end   <= HF_out_w.tlast;
  --         proc_end_d <= proc_end;
  --     end if;
  -- end if;
  -- end process;
  hit_finder_rst <= rst or reset;-- or localreset;


  -----------------------------------------------------------------------
  -- HLS hitfinder
  -----------------------------------------------------------------------
  HF_inst: entity work.hit_finder
    port map(
      clk           => clk,    
      rst           => hit_finder_rst, 
      s_axis_data_w => FIR_out_w,  
      s_axis_data_r => FIR_out_r,
      m_axis_data_w => HF_out_w,
      m_axis_data_r => HF_out_r    
    );


  -----------------------------------------------------------------------
  -- Generate clock
  -----------------------------------------------------------------------
  clock_gen : process
  begin

    while not s_eof loop
      clk          <= '0', '1' after CLOCK_PERIOD / 2;
      wait for CLOCK_PERIOD;
      cycleCounter <= cycleCounter +1;
    end loop;
    wait;

  end process clock_gen;

  -- To Do - process to generate start_reading , start_writing flags in a more elegant way
  ctrl_flag_gen: process
  begin
    -- Start reading/writing from/to file
    reset <= '0'; 
    s_start_reading <= '0';
    s_start_writing <= '0';
    
    wait for CLOCK_PERIOD*10;

    reset <= '1';
    wait for CLOCK_PERIOD*10;
    reset <= '0';

    wait for CLOCK_PERIOD*20;  
    s_start_reading <= '1';
    s_start_writing <= '1';

    --wait for CLOCK_PERIOD*790; --790 813
    --reset <= '1';
    --wait for CLOCK_PERIOD*3;
    --reset <= '0';

    wait;
  end process ctrl_flag_gen;


   -----------------------------------------------------------------------
   -- Instantiate file that reads from file and writes to AXI4S
   -----------------------------------------------------------------------
  dtpc_sim_src_inst : entity work.dtpc_sim_src
    generic map(
        IFILE        => IFILE,
        CLOCK_PERIOD => CLOCK_PERIOD
    )
    port map (
        clk_i           => clk,                 --! Rising edge active
        start_reading_i => s_start_reading,     --! Rising edge triggers start of file reading.
        eof_o           => s_eof,               --! Goes high when end of file reached
        q_o             => s_from_src_to_block, --! AXI4S signals from block ( TDATA, TVALID, etc. )
        d_i             =>  s_from_block_to_src --! AXI4S signals from downstream ( TREADY )
    );


  -----------------------------------------------------------------------
  -- Instantiate file that reads from AXI4S  and writes to file
  -----------------------------------------------------------------------
  dtpc_sim_sink_inst : entity work.dtpc_sim_sink
  generic map(
      OFILE                      => OFILE,
      G_TOGGLE_TREADY            => G_TOGGLE_TREADY,
      G_MAX_TREADY_DEASSERT_TIME => G_MAX_TREADY_DEASSERT_TIME,
      G_MAX_TREADY_REASSERT_TIME => G_MAX_TREADY_REASSERT_TIME
  )
  port map (
      clk_i           => clk,                   --! Rising edge active
      start_writing_i => s_start_writing,       --! Rising edge triggers start of file reading.
      q_o             => s_from_sink_to_block,  --! AXI4S signals to block( TREADY )
      d_i             =>  s_from_block_to_sink  --! AXI4S signals from block ( TDATA, TVALID, etc. )
  );




end tb;
