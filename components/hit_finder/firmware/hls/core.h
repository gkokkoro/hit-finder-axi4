#ifndef core_h
#define core_h

#include <ap_int.h>
#include <ap_axi_sdata.h>
#include <hls_stream.h>

typedef ap_uint<16> word;

#define NSAMPLES 64
#define MAXHITS 13
#define VALUES_IN_HIT 2
#define THRESHOLD 20
#define LAST_ON_CONTINUE 1

typedef ap_axiu<16, 1, 0, 0> ap_axis_str;


void hit_finder(hls::stream<ap_axis_str> &input_stream, hls::stream<ap_axis_str> &output_stream);

#endif
