#include "core.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <map>


std::string read_file_into_string(const std::string& path) {
	auto ss = std::ostringstream{};
	std::ifstream input_file(path);
	ss << input_file.rdbuf();
	return ss.str();
}

std::vector<word> read_data(int mock_number){
	std::vector<word> data(NSAMPLES);
	std::string filename;
	filename ="../../../../../../src/hit-finder-axi4/components/hit_finder/firmware/hls_tb/mock_data/mock_data_";
	filename += std::to_string(mock_number);
	filename += ".csv";
	std::string file_contents;
	char delimiter = ',';

	file_contents = read_file_into_string(filename);
	int count = 0;
	size_t pos = 0;
	std::string token;
	int temp;
	while ((pos = file_contents.find(delimiter)) != std::string::npos) {
		token = file_contents.substr(0, pos);

		std::stringstream degree(token);
		degree >> temp;
		data[count] = temp;
		count++;
		file_contents.erase(0, pos + 1);
	}
	std::stringstream degree(file_contents);
	degree >> temp;
	data[count] = temp;
	return(data);
}

int main(){
	std::cout<<"\n===========================\n";

	std::vector<int> data_files{1,-10,4,8,11,5};

	std::vector<std::vector<word>> data;
	for(std::size_t it = 0; it < data_files.size(); ++it) {
		data.push_back(read_data(data_files[it]));
	}



	hls::stream<ap_axis_str> input_stream;
	hls::stream<ap_axis_str> output_stream;

	for(std::size_t it = 0; it < data.size(); ++it) {
		if (it!=0){
			std::cout<<"\n";
		}
		std::cout<<"Data: ";
		for (int i = 0;i<NSAMPLES-1; i++){
			std::cout<<data[it][i]<<", ";
		}
		std::cout<<data[it][NSAMPLES-1]<<"\n";
		for (int i=0; i<NSAMPLES;i++){
			ap_axis_str strm_val_in;
			strm_val_in.data  = data[it][i];
			strm_val_in.keep  = 3;
			strm_val_in.user  = false;
			strm_val_in.last  = (i == NSAMPLES-1);

			input_stream<<strm_val_in;
		}

		hit_finder(input_stream, output_stream);

		while(!output_stream.empty()) {
			ap_axis_str strm_val_out;
			output_stream.read(strm_val_out);
			std::cout<<strm_val_out.data;
				if (strm_val_out.last) {
					std::cout<<"\n";
				}else if (strm_val_out.user){
					std::cout<<" || ";
				} else {
					std::cout<<", ";
				}
		}
	}


	std::cout<<"===========================\n\n";
	return(0);
}
