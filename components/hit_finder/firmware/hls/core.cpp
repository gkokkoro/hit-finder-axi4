#include "core.h"
#ifndef __SYNTHESIS__
#include <iostream>
#endif

ap_axis_str word_to_stream(word false_stream, int user = 0, int last = 0){
#pragma HLS inline
	ap_axis_str converted;
	converted.data = false_stream;
	converted.keep = 3;
	converted.user = user;
	converted.last = last;
	return(converted);
}

void hit_finder(hls::stream<ap_axis_str> &input_stream, hls::stream<ap_axis_str> &output_stream){
#pragma hls dataflow
#pragma HLS INTERFACE axis port=input_stream
#pragma HLS INTERFACE axis port=output_stream
#pragma HLS INTERFACE ap_ctrl_none port=return

	word adc_values[NSAMPLES];
	for (int i=0; i<NSAMPLES; i++){
		adc_values[i] = input_stream.read().data;
	}

	word hit_values[6];
	hit_values[5]=0;
	word sum_value = 0;
	#pragma HLS array_partition variable=hit_values
	bool hit = false;
	bool prev_hit;
	int number_of_hits = 0;
	bool record_output = false;
	bool last_cont = 0;


	word st_buffer_list[MAXHITS];
	word et_buffer_list[MAXHITS];
	word pt_buffer_list[MAXHITS];
	word pv_buffer_list[MAXHITS];
	word sv_buffer_list[MAXHITS];
	word hc_buffer_list[MAXHITS];

	for (int i = 0; i < NSAMPLES; i++) {

		prev_hit = hit;
		hit = (adc_values[i]>THRESHOLD) and not (adc_values[i] & (1 << 15));

		if (hit){
			if(prev_hit){
				if  (adc_values[i]>hit_values[3]){
					hit_values[3]=adc_values[i];
					hit_values[2]=i;
				}
			}else{
				hit_values[0] = i;
				hit_values[3]=adc_values[i];
				hit_values[2]=i;
			}
			hit_values[1] = i;
			sum_value += adc_values[i];

			if ((i == NSAMPLES-1)&&(hit_values[1]-hit_values[0])>(VALUES_IN_HIT-2)){
				hit_values[5] = 1;
				hit_values[4] = sum_value;
				record_output = true;
				last_cont = 1;
			}
		} else if (prev_hit){
			if ((hit_values[1]-hit_values[0])>(VALUES_IN_HIT-2)){
				hit_values[4] = sum_value;
				record_output = true;

			}
			sum_value = 0;
		}
		if (record_output) {
			st_buffer_list[number_of_hits] = hit_values[0];
			et_buffer_list[number_of_hits] = hit_values[1];
			pt_buffer_list[number_of_hits] = hit_values[2];
			pv_buffer_list[number_of_hits] = hit_values[3];
			sv_buffer_list[number_of_hits] = hit_values[4];
			hc_buffer_list[number_of_hits] = hit_values[5];

			record_output = false;
			number_of_hits++;
		}
	}
	if (number_of_hits>0){
		int max_counter = number_of_hits;
		int k = 0;

		while (k<max_counter){
		#pragma HLS loop_tripcount min=1 max=13 avg=2
		#pragma HLS pipeline II=6
			bool write_last = last_cont and (k == (max_counter-1)) and LAST_ON_CONTINUE;
			output_stream.write(word_to_stream(st_buffer_list[k], 0, 0));
			output_stream.write(word_to_stream(et_buffer_list[k], 0, 0));
			output_stream.write(word_to_stream(pv_buffer_list[k], 0, 0));
			output_stream.write(word_to_stream(pt_buffer_list[k], 0, 0));
			output_stream.write(word_to_stream(sv_buffer_list[k], 0, 0));
			output_stream.write(word_to_stream(hc_buffer_list[k], 1, write_last));
			k++;
		}
	}
	if (not (last_cont and LAST_ON_CONTINUE)){
		ap_axis_str no_hit_out;
		no_hit_out.data = 0;
		no_hit_out.keep = 0;
		no_hit_out.last = 1;
		no_hit_out.user = 0;
		output_stream.write(no_hit_out);
	}

}


