library ieee;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use work.dtpc_stream_defs.all;
library xil_defaultlib;

entity hit_finder is
  port (
    clk            : in  std_logic;
    rst            : in  std_logic;
    s_axis_data_w  : in  dtpc_axis4_w;
    s_axis_data_r  : out dtpc_axis4_r;
    m_axis_data_w  : out dtpc_axis4_w;
    m_axis_data_r  : in  dtpc_axis4_r
  );
end hit_finder;

architecture rtl of hit_finder is
  signal s_axis_tlast :std_logic_vector( 0 downto 0) := (others => '0');
  signal m_axis_tlast :std_logic_vector( 0 downto 0) := (others => '0');
  signal s_axis_tstrb :std_logic_vector (1 downto 0) := (others => '0');
  signal m_axis_tstrb :std_logic_vector (1 downto 0) := (others => '0');
  signal rst_n :std_logic := '1';
  component ORGATE
  port (A,B : in  bit;
        Z : out bit);
  end component;

begin
  rst_n <= not rst;
  s_axis_tlast(0) <= s_axis_data_w.tlast;
  hit_finder_inst : entity xil_defaultlib.hit_finder
    port map(
        ap_clk                 => clk,
        ap_rst_n               => rst_n,
        input_stream_TVALID    => s_axis_data_w.tvalid,
        input_stream_TSTRB     => s_axis_tstrb,
        input_stream_TREADY    => s_axis_data_r.tready,
        input_stream_TLAST     => s_axis_tlast,--s_axis_data_w.tlast,
        input_stream_TUSER     => s_axis_data_w.tuser,
        input_stream_TKEEP     => s_axis_data_w.tkeep,
        input_stream_TDATA     => s_axis_data_w.tdata(15 downto 0),
        output_stream_TVALID   => m_axis_data_w.tvalid,
        output_stream_TSTRB    => m_axis_tstrb,
        output_stream_TREADY   => m_axis_data_r.tready,
        output_stream_TLAST    => m_axis_tlast,--m_axis_data_w.tlast, Fix this
        output_stream_TUSER    => m_axis_data_w.tuser,
        output_stream_TKEEP    => m_axis_data_w.tkeep,
        output_stream_TDATA    => m_axis_data_w.tdata(15 downto 0)
      );
  
   m_axis_data_w.tlast <=  m_axis_tlast(0);

end rtl;

