# hit finder AXI4

3rd attempt at a hit finder
Goals:
Make a hit finder that is both pipelined and uses the AXI4 protocol

```
curl -L https://github.com/ipbus/ipbb/archive/dev/2021i.tar.gz | tar xvz
source ipbb-dev-2021i/env.sh
ipbb init hls-hf
cd hls-hf
ipbb add git https://:@gitlab.cern.ch:8443/dune-daq/readout/dtp-firmware.git
ipbb add git https://github.com/ipbus/ipbus-firmware.git -b v1.10
ipbb add git https://:@gitlab.cern.ch:8443/gkokkoro/hit-finder-axi4.git -b
ipbb add git ssh://git@gitlab.cern.ch:7999/dune-daq/readout/dtp-patterns.git
ipbb proj create vitis-hls hls_hf hit-finder-axi4:components/hit_finder hit_finder_ip_hls.d3
cd proj/hls_hf/
ipbb vitis-hls generate-project
ipbb vitis-hls csim
ipbb vitis-hls csynth
ipbb vitis-hls cosim
ipbb vitis-hls export-ip
ipbb proj create sim hls_hf_sim hit-finder-axi4:project tb_dtp_proc.d3 
cd ../hls_hf_sim/
ipbb sim setup-simlib
ipbb sim ipcores 
ipbb sim generate-project 
./run_sim -GIFILE=../../src/dtp-patterns/FixedHits/A/FixedHits_A_fir6_up_axi4s.0.txt -GOFILE=./output.txt
```